.\" This file is part of grecs -*- nroff -*-
.\" Copyright (C) 2007-2022 Sergey Poznyakoff
.\"
.\" Grecs is free software; you can redistribute it and/or modify
.\" it under the terms of the GNU General Public License as published by
.\" the Free Software Foundation; either version 3, or (at your option)
.\" any later version.
.\"
.\" Grecs is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.\" GNU General Public License for more details.
.\"
.\" You should have received a copy of the GNU General Public License
.\" along with Grecs.  If not, see <http://www.gnu.org/licenses/>.
.\"
.TH GRECS_CONFIG 3 "December 25, 2014" "GRECS" "Grecs User Reference"
.SH NAME
\fBGrecs\fR configuration file syntax 
.SH DESCRIPTION
.PP
A configuration file consists of statements and comments.
.PP
There are three classes of lexical tokens: keywords, values, and
separators. Blanks, tabs, newlines and comments, collectively called
\fIwhite space\fR are ignored except as they serve to separate
tokens. Some white space is required to separate otherwise adjacent 
keywords and values.
.SH COMMENTS
Comments may appear anywhere where white space may appear in the
configuration file.  There are two kinds of comments:
single-line and multi-line comments.  Single-line comments start
with
.B #
or
.B //
and continue to the end of the line:
.sp
.RS 4
.nf
# This is a comment
// This too is a comment
.fi
.RE
.PP
\fIMulti-line\fB or \fIC-style\fB comments start with the two
characters
.B /*
(slash, star) and continue until the first occurrence of
.B */
(star, slash).
.PP
Multi-line comments cannot be nested.  However, single-line comments
may well appear within multi-line ones.
.SS "Pragmatic Comments"
Pragmatic comments are similar to usual single-line comments,
except that they cause some changes in the way the configuration is
parsed.  Pragmatic comments begin with a
.B #
sign and end with the next physical newline character.
.TP
.BR "#include <" "file" >
.PD 0
.TP
.BR "#include " "file"
.PD
Include the contents of the file \fIfile\fR.  There are three possible
use cases.

If \fIfile\fR is an absolute file name, the named file is included.
An error message will be issued if it does not exist.

If \fIfile\fR contains wildcard characters (\fB*\fR, \fB[\fR,
\fB]\fR or \fB?\fR), it is interpreted as shell globbing pattern and
all files matching that pattern are included, in lexicographical
order.  If no files match the pattern, the statement is silently
ignored.

Otherwise, the form with angle brackets searches for file in the
\fIinclude  search path\fR, while the second one looks for it in the
current working directory first, and, if not found there, in the
include search path.  If the file is not found, an error message will
be issued.
.sp
The default include search path is:
.sp
\fIprefix\fR/share/\fIPROGNAME\fR/\fIVERSION\fR/include
.br
\fIprefix\fR/share/\fIPROGNAME\fR/include
.sp
where \fIprefix\fR stands for the installation prefix (normally
\fB/usr\fR or \fB/usr/local\fR), \fIPROGNAME\fR stands for the name of
the program which uses the configuration file, and \fIVERSION\fR for
its version number.
.sp
The include search path can be modified using the
.BI grecs_include_path_setup ()
and
.BI grecs_include_path_setup_v ()
functions. Refer to 
.BR grecs_include_path_setup (3),
for a detailed discussion.
.TP
.BR "#include_once <" "file" >
.PD 0
.TP
.BR "#include_once " "file"
.PD
Same as \fB#include\fR, except that, if the \fIfile\fR has already
been included, it will not be included again.
.TP
.BR "#line " "num"
.PD 0
.TP
.BR "#line " "num" " \(dq" "file" "\(dq"
.PD
This line causes the parser to believe, for purposes of error
diagnostics, that the line number of the next source line
is given by \fInum\fR and the current input file is named by
\fIfile\fR. If the latter is absent, the remembered file name
does not change.
.TP
.BR "# " "num" " \(dq" "file" "\(dq"
This is a special form of the \fB#line\fR statement, understood for
compatibility with the C preprocessor.
.PP
These statements provide a rudimentary preprocessing
features.  For more sophisticated ways to modify configuration before
parsing, see \fBPREPROCESSOR\fR.
.SH STATEMENTS
.SS "Simple statement"
A \fIsimple statement\fR consists of a keyword and value
separated by any amount of whitespace.  Simple statement is terminated
with a semicolon (\fB;\fR).
.PP
The following is a simple statement:
.sp
.RS 4
.nf
standalone yes;
pidfile /var/run/slb.pid;
.RE
.fi
.PP
A \fIkeyword\fR begins with a letter and may contain letters,
decimal digits, underscores (\fB_\fR) and dashes (\fB-\fR).
Examples of keywords are:
.sp
.RS 4
.nf
expression
output-file
.RE
.fi
.PP
A \fIvalue\fR can be one of the following:
.TP
.I number
A number is a sequence of decimal digits.
.TP
.I boolean
A boolean value is one of the following: \fByes\fR, \fBtrue\fR,
\fBt\fR or \fB1\fR, meaning \fItrue\fR, and \fBno\fR,
\fBfalse\fR, \fBnil\fR, \fB0\fR meaning \fIfalse\fR.
.TP
.I unquoted string
An unquoted string may contain letters, digits, and any of the
following characters: \fB_\fR, \fB\-\fR, \fB.\fR, \fB/\fR,
\fB@\fR, \fB*\fR, \fB:\fR.
.TP
.I quoted string
A quoted string is any sequence of characters enclosed in
double-quotes (\fB\(dq\fR).  A backslash appearing within a quoted
string introduces an \fIescape sequence\fR, which is replaced
with a single character according to the following rules:
.nf
.ul
	Sequence	Replaced with
	\\a		Audible bell character (ASCII 7)
	\\b		Backspace character (ASCII 8)
	\\f		Form-feed character (ASCII 12)
	\\n		Newline character (ASCII 10)
	\\r		Carriage return character (ASCII 13)
	\\t		Horizontal tabulation character (ASCII 9)
	\\v		Vertical tabulation character (ASCII 11)
	\\\\		A single backslash
	\\\(dq		A double-quote.
.fi
In addition, the sequence \fB\\\fInewline\fR is removed from
the string.  This allows to split long strings over several
physical lines, e.g.:
.sp
.nf
    "a long string may be\\
     split over several lines"
.fi
.sp
If the character following a backslash is not one of those specified
above, the backslash is ignored and a warning is issued.

Two or more adjacent quoted strings are concatenated, which gives
another way to split long strings over several lines to improve
readability.  The following fragment produces the same result as the
example above:
.sp
.nf
    "a long string may be"
    " split over several lines"
.fi
.TP
.I Here-document
A \fIhere-document\fR is a special construct that allows to introduce
strings of text containing embedded newlines.  

The
.BI "<<" "word"
construct instructs the parser to read all the following lines up to
the line containing only \fIword\fR, with possible trailing blanks.
Any lines thus read are concatenated together into a single string.
For example: 
.sp
.nf
    <<EOT
    A multiline
    string
    EOT
.fi
.sp
The body of a here-document is interpreted the same way as a
double-quoted string, unless \fIword\fR is preceded by a backslash
(e.g.  \fB<<\\EOT\fR) or enclosed in double-quotes, in which case
the text is read as is, without interpretation of escape sequences.

If \fIword\fR is prefixed with \fB\-\fR (a dash), then all leading
tab characters are stripped from input lines and the line containing
\fIword\fR.  Furthermore, \fB\-\fR is followed by a single space,
all leading whitespace is stripped from them.  This allows to indent
here-documents in a natural fashion.  For example:
.sp
.nf
    <<- TEXT
        The leading whitespace will be
        ignored when reading these lines.
    TEXT
.fi
.sp
It is important that the terminating delimiter be the only token on
its line.  The only exception to this rule is allowed if a
here-document appears as the last element of a statement.  In this
case a semicolon can be placed on the same line with its terminating 
delimiter, as in: 
.sp
.nf
    help-text <<-EOT
        A sample help text.
    EOT;
.fi
.TP
.I list
A comma-separated list of values, enclosed in parentheses.  The
following example shows a statement whose value is a list of strings:
.sp
.nf
    alias (test, null);
.fi
.sp
In any context where a list is appropriate, a single value is allowed
without being a member of a list: it is equivalent to a list with a
single member.  This means that, e.g.
.sp
.nf
    alias test;
.fi
.sp
is equivalent to
.sp
.nf
    alias (test);
.fi
.sp
provided that the \fBalias\fR statement is defined as taking a list as
its argument.
.TP
.I block statement
A block statement introduces a logical group of 
statements.  It consists of a keyword, followed by an optional value,
called a \fBtag\fR, and a sequence of statements enclosed in curly
braces, as shown in the example below:
.sp
.nf
    server srv1 {
        host 10.0.0.1;
        community "foo";
    }
.fi
.sp
The closing curly brace may be followed by a semicolon, although
this is not required.
.SS PREPROCESSOR
Before being parsed, the configuration file is preprocessed.
The built-in preprocessor handles only file inclusion
and
.B #line
statements, while the rest of traditional preprocessing facilities,
such as macro expansion, is supported via
.BR m4 (1),
which is used as external preprocessor.
.PP
The external preprocessor is invoked with the \fB-s\fR flag, which
instructs it to include line synchronization information in its
output.  This information is then used by the parser to display meaningful
diagnostic.
.PP
An initial set of macro definitions is supplied in the file named
\fBpp-setup\fR file, which is located in
\fIprefix\fB/share/\fIPROGNAME\fR/\fIVERSION\fR/include}
directory.
.PP
The default \fBpp-setup\fR file renames all m4 built-in
macro names so they all start with the prefix \fBm4_\fI.  This
is similar to GNU m4 \fB--prefix-builtin\fR option, but has an
advantage that it works with non-GNU m4 implementations as
well.
.SH "SEE ALSO"
.BR m4 (1),
.BR grecs_parse (3),
.BR grecs_include_path_setup (3),
.BR grecs_tree_process (3).
.SH AUTHORS
Sergey Poznyakoff
.SH "BUG REPORTS"
Report bugs to <gray+grecs@gnu.org.ua>.
.SH COLOPHON
The \fBGrecs\fR library is constantly changing, so this manual page
may be incorrect or out-of-date.  For the latest copy of \fBGrecs\fR
documentation, visit <http://www.gnu.org.ua/software/grecs>.
.SH COPYRIGHT
Copyright \(co 2011 Sergey Poznyakoff
.br
.na
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
.br
.ad
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
.\" Local variables:
.\" eval: (add-hook 'write-file-hooks 'time-stamp)
.\" time-stamp-start: ".TH [A-Z_][A-Z0-9_]* [0-9] \""
.\" time-stamp-format: "%:B %:d, %:y"
.\" time-stamp-end: "\""
.\" time-stamp-line-limit: 20
.\" end:
