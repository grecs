@c This file is part of grecs - Gray's Extensible Configuration System
@c Copyright (C) 2007--2022 Sergey Poznyakoff
@c
@c Grecs is free software; you can redistribute it and/or modify
@c it under the terms of the GNU General Public License as published by
@c the Free Software Foundation; either version 3, or (at your option)
@c any later version.
@c
@c Grecs is distributed in the hope that it will be useful,
@c but WITHOUT ANY WARRANTY; without even the implied warranty of
@c MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
@c GNU General Public License for more details.
@c
@c You should have received a copy of the GNU General Public License
@c along with Grecs.  If not, see <http://www.gnu.org/licenses/>.

@c TIPS: 1. You can @include this file directly into your Texinfo
@c document.  It produces a section as its upper level sectioning
@c command.  If it is not appropriate, use @raisesections or
@c @lowersections:
@c
@c   @raisesections
@c   @include grecs-syntax.texi
@c   @lowersections
@c
@c 2. This texinfo source refers to the following values:
@c
@c  PACKAGE     - name of the package
@c  PROGNAME    - name of the program, which uses Grecs (without any
@c                Texinfo markup)
@c
@c 3. Some additional/optional parts of the text are commented out, and
@c marked with `@c FIXME:' comments.

  The configuration file consists of statements and comments.

  There are three classes of lexical tokens: keywords, values, and
separators.  Blanks, tabs, newlines and comments, collectively called
@dfn{white space} are ignored except as they serve to separate
tokens.  Some white space is required to separate otherwise adjacent 
keywords and values.

@menu
* Comments::
* Pragmatic Comments::
* Statements::
* Preprocessor::
@end menu

@node Comments
@subsection Comments
@cindex Comments in a configuration file
@cindex single-line comments
  @dfn{Comments} may appear anywhere where white space may appear in the
configuration file.  There are two kinds of comments:
single-line and multi-line comments.  @dfn{Single-line} comments start
with @samp{#} or @samp{//} and continue to the end of the line:

@smallexample
# This is a comment
// This too is a comment
@end smallexample

@cindex multi-line comments
  @dfn{Multi-line} or @dfn{C-style} comments start with the two
characters @samp{/*} (slash, star) and continue until the first
occurrence of @samp{*/} (star, slash).

  Multi-line comments cannot be nested.  However, single-line comments
may well appear within multi-line ones.

@node Pragmatic Comments
@subsection Pragmatic Comments
@cindex comments, pragmatic
@cindex pragmatic comments
  Pragmatic comments are similar to usual single-line comments,
except that they cause some changes in the way the configuration is
parsed.  Pragmatic comments begin with a @samp{#} sign and end with the
next physical newline character.

@deffn {Pragma} #include <@var{file}>
@deffnx {Pragma} #include @var{file}
Include the contents of the file @var{file}.  There are three possible
use cases.

If @var{file} is an absolute file name, the named file is included.
An error message will be issued if it does not exist.

If @var{file} contains wildcard characters (@samp{*}, @samp{[},
@samp{]} or @samp{?}), it is interpreted as shell globbing pattern and
all files matching that pattern are included, in lexicographical
order.  If no files match the pattern, the statement is silently
ignored.

Otherwise, the form with angle brackets searches for file in the
@dfn{include  search path}, while the second one looks for it in the
current working directory first, and, if not found there, in the
include search path.  If the file is not found, an error message will
be issued.

The default include search path is:

@enumerate 1
@item @file{@var{prefix}/share/@value{PROGNAME}/include}
@item @file{@var{prefix}/share/@value{PROGNAME}/@value{VERSION}/include}
@end enumerate

@noindent
where @var{prefix} is the installation prefix.

@c FIXME: Uncomment this, if necessary:
@ignore
New directories can be appended in front of it using @option{-I}
(@option{--include-directory}) command line option
(@pxref{Preprocessor, include-directory}).  
}
@end ignore
@end deffn

@deffn {Pragma} #include_once <@var{file}>
@deffnx {Pragma} #include_once @var{file}
  Same as @code{#include}, except that, if the @var{file} has already
been included, it will not be included again.
@end deffn

@deffn {Pragma} #line @var{num}
@deffnx {Pragma} #line @var{num} "@var{file}"
  This line causes the parser to believe, for purposes of error
diagnostics, that the line number of the next source line is given by
@var{num} and the current input file is named by @var{file}.
If the latter is absent, the remembered file name does not change.
@end deffn

@deffn {Pragma} # @var{num} "@var{file}"
  This is a special form of @code{#line} statement, understood for
compatibility with the @sc{c} preprocessor.
@end deffn

@deffn {Pragma} #warning "@var{text}"
Emit a warning message @var{text} in the current location.
@end deffn

@deffn {Pragma} #error "@var{text}"
Emit error message @var{text} in the current location and continue
parsing.  The error counter is increased.  It is
implementation-dependent whether this error will cause the
configuration to be rejected as invalid.
@end deffn

@deffn {Pragma} #abend "@var{text}"
Emit error message @var{text} in the current location and terminate
further parsing.  This statement is guaranteed to abort the processing
of the configuration file and to reject it due to syntax errors.
@end deffn

  In fact, these statements provide a rudimentary preprocessing
features.  For more sophisticated ways to modify configuration before
parsing, see @ref{Preprocessor}.

@node Statements
@subsection Statements
@cindex statements, configuration file
@cindex configuration file statements
@cindex statement, simple
@cindex simple statements
  A @dfn{simple statement} consists of a keyword and value
separated by any amount of whitespace.  Simple statement is terminated
with a semicolon (@samp{;}).

  The following is a simple statement:

@smallexample
standalone yes;
pidfile /var/run/slb.pid;
@end smallexample

  A @dfn{keyword} begins with a letter and may contain letters,
decimal digits, underscores (@samp{_}) and dashes (@samp{-}).
Examples of keywords are: @samp{expression}, @samp{output-file}.

  A @dfn{value} can be one of the following:

@table @asis
@item number
  A number is a sequence of decimal digits.

@item boolean
@cindex boolean value
  A boolean value is one of the following: @samp{yes}, @samp{true},
@samp{t} or @samp{1}, meaning @dfn{true}, and @samp{no},
@samp{false}, @samp{nil}, @samp{0} meaning @dfn{false}.
  
@item unquoted string
@cindex string, unquoted
  An unquoted string may contain letters, digits, and any of the
following characters: @samp{_}, @samp{-}, @samp{.}, @samp{/},
@samp{@@}, @samp{*}, @samp{:}.

@item quoted string
@cindex quoted string
@cindex string, quoted
@cindex escape sequence
  A quoted string is any sequence of characters enclosed in
double-quotes (@samp{"}).  A backslash appearing within a quoted
string introduces an @dfn{escape sequence}, which is replaced
with a single character according to the following rules:

@float Table, backslash-interpretation
@caption{Backslash escapes}
@multitable @columnfractions 0.30 .5
@item Sequence @tab Replaced with
@item \a @tab Audible bell character (@acronym{ASCII} 7)
@item \b @tab Backspace character (@acronym{ASCII} 8)
@item \f @tab Form-feed character (@acronym{ASCII} 12)
@item \n @tab Newline character (@acronym{ASCII} 10)
@item \r @tab Carriage return character (@acronym{ASCII} 13)
@item \t @tab Horizontal tabulation character (@acronym{ASCII} 9)
@item \v @tab Vertical tabulation character (@acronym{ASCII} 11)
@item \\ @tab A single backslash (@samp{\})
@item \" @tab A double-quote.
@end multitable
@end float

  In addition, the sequence @samp{\@var{newline}} is removed from
the string.  This allows to split long strings over several
physical lines, e.g.:

@smallexample
@group
"a long string may be\
 split over several lines"
@end group
@end smallexample

  If the character following a backslash is not one of those specified
above, the backslash is ignored and a warning is issued.

@c FIXME: If grecs_parser_options variable has
@c FIXME: GRECS_OPTION_QUOTED_STRING_CONCAT bit set, then the
@c FIXME: following holds:

@ignore
  Two or more adjacent quoted strings are concatenated, which gives
another way to split long strings over several lines to improve
readability.  The following fragment produces the same result as the
example above:

@smallexample
@group
"a long string may be"
" split over several lines"
@end group
@end smallexample
@end ignore

@anchor{here-document}
@item Here-document
@cindex here-document
  A @dfn{here-document} is a special construct that allows to introduce
strings of text containing embedded newlines.  

  The @code{<<@var{word}} construct instructs the parser to read all
the following lines up to the line containing only @var{word}, with
possible trailing blanks.  Any lines thus read are concatenated
together into a single string.  For example:

@smallexample
@group
<<EOT
A multiline
string
EOT
@end group
@end smallexample

  The body of a here-document is interpreted the same way as a
double-quoted string, unless @var{word} is preceded by a backslash
(e.g.  @samp{<<\EOT}) or enclosed in double-quotes, in which case
the text is read as is, without interpretation of escape sequences.

  If @var{word} is prefixed with @code{-} (a dash), then all leading
tab characters are stripped from input lines and the line containing
@var{word}.  Furthermore, if @code{-} is followed by a single space,
all leading whitespace is stripped from them.  This allows to indent
here-documents in a natural fashion.  For example:

@smallexample
@group
<<- TEXT
    The leading whitespace will be
    ignored when reading these lines.
TEXT
@end group
@end smallexample

  It is important that the terminating delimiter be the only token on
its line.  The only exception to this rule is allowed if a
here-document appears as the last element of a statement.  In this
case a semicolon can be placed on the same line with its terminating 
delimiter, as in: 

@smallexample
help-text <<-EOT
        A sample help text.
EOT;
@end smallexample

@item list
@cindex list
  A @dfn{list} is a comma-separated list of values.  Lists are
enclosed in parentheses.  The following example shows a statement
whose value is a list of strings:

@smallexample
alias (test,null);
@end smallexample

  In any case where a list is appropriate, a single value is allowed
without being a member of a list: it is equivalent to a list with a
single member.  This means that, e.g.

@smallexample
alias test;
@end smallexample

@noindent
is equivalent to

@smallexample
alias (test);
@end smallexample
@end table

@cindex statement, block
@cindex block statement
  A @dfn{block statement} introduces a logical group of 
statements.  It consists of a keyword, followed by an optional value,
and a sequence of statements enclosed in curly braces, as shown in
the example below:

@smallexample
@group
server srv1 @{
  host 10.0.0.1;
  community "foo";
@}
@end group
@end smallexample

  The closing curly brace may be followed by a semicolon, although
this is not required.

@node Preprocessor
@subsection Preprocessor
@cindex preprocessor
@cindex m4
  Before actual parsing, the configuration file is preprocessed.
The built-in preprocessor handles only file inclusion
and @code{#line} statements (@pxref{Pragmatic Comments}), while the
rest of traditional preprocessing facilities, such as macro expansion,
is supported via @command{m4}, which serves as external preprocessor. 

  The detailed description of @command{m4} facilities lies far beyond
the scope of this document.  You will find a complete user manual in
@ifnothtml
@ref{Top, GNU M4 manual, GNU M4, m4, GNU M4 macro processor}.
@end ifnothtml
@ifhtml
@uref{http://www.gnu.org/software/m4/manual}.
@end ifhtml
For the rest of this subsection we assume the reader is sufficiently
acquainted with @command{m4} macro processor.

@cindex @file{pp-setup}
  The external preprocessor is invoked with @option{-s} flag, which
instructs it to include line synchronization information in its
output.  This information is then used by the parser to display meaningful
diagnostic.

An initial set of macro definitions is supplied by the 
@file{pp-setup} file, located in
@file{@var{prefix}/share/@value{PROGNAME}/@value{VERSION}/include}
directory.

  The default @file{pp-setup} file renames all @command{m4} built-in
macro names so they all start with the prefix @samp{m4_}.  This
is similar to GNU m4 @option{--prefix-builtin} option, but has an
advantage that it works with non-GNU @command{m4} implementations as
well.

@c FIXME:
@ignore
  Additional control over the preprocessor is provided via the
following command line options:

@table @option
@xopindex{define, introduced}
@sopindex{D, introduced}
@item --define=@var{name}[=@var{value}]
@itemx -D@var{name}[=@var{value}]
  Define the preprocessor symbol @var{name} as having @var{value}, or
empty.

@xopindex{include-directory, introduced}
@sopindex{I, introduced}
@item --include-directory=@var{dir}
@itemx -I@var{dir}
  Add @var{dir} to the list of directories searched for preprocessor
include files.

@xopindex{no-preprocessor, defined}
@item --no-preprocessor
  Disable preprocessor.

@xopindex{preprocessor, defined}
@item --preprocessor=@var{command}
Use @var{command} instead of the default preprocessor.
@end table
@end ignore

