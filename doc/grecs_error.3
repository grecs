.\" This file is part of grecs -*- nroff -*-
.\" Copyright (C) 2007-2022 Sergey Poznyakoff
.\"
.\" Grecs is free software; you can redistribute it and/or modify
.\" it under the terms of the GNU General Public License as published by
.\" the Free Software Foundation; either version 3, or (at your option)
.\" any later version.
.\"
.\" Grecs is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.\" GNU General Public License for more details.
.\"
.\" You should have received a copy of the GNU General Public License
.\" along with Grecs.  If not, see <http://www.gnu.org/licenses/>.
.\"
.TH GRECS_ERROR 3 "July 1, 2011" "GRECS" "Grecs User Reference"
.SH NAME
grecs_error, grecs_warning \- grecs error reporting functions
.SH SYNOPSIS
.nf
.B #include <grecs.h>
.sp
.BI "void grecs_error(grecs_locus_t " "*locus" ", int " "errnum"  ", "
.ti +17
.BI       "const char " "*format" ", ...);"
.br
.BI "void grecs_warning(grecs_locus_t " "*locus" ", int " "errnum" ", "
.ti +17
.BI       "const char " "*format" ", ...);"
.sp
.BI "void (*grecs_print_diag_fun)(grecs_locus_t " "*locus" ", "
.ti +17
.BI       "int " "err" ", int " "errnum" ", const char " "*msg" );
.sp
.B extern int grecs_error_count;
.SH DESCRIPTION
.BI grecs_error ()
is a error reporting function for \fBgrecs\fR library.  It flushes
stdout, and outputs to stderr the file name and line number, as
supplied by the 
.BI locus
argument, a colon and a space, the message specified by the
.BR printf (3)-style
format string \fIformat\fR, and, if \fIerrnum\fR is non-zero, a second
colon and a space followed by the string given by
.BI perror (errnum).
Any arguments required for format should follow \fIformat\fR in the
argument list. The output is terminated by a newline character.
.PP
If \fIlocus\fR is \fBNULL\fR, it is ignored and no file location is
printed.
.PP
Each call to
.BI grecs_error ()
increments the value of the
.BI grecs_error_count
global variable.
The
.BI grecs_warning ()
function works the same as
.BI grecs_error (),
except that it outputs, before the formatted message and after the
file location, the word \fBwarning\fR, followed by a semicolon
and a space character.  It does not modify the
.BI grecs_error_count
variable.
.PP
Calling program can modify the behavior of both functions by setting
.BI grecs_print_diag_fun
to the address of a custom error-reporting function. This function is
called with the following arguments:
.TP
.I locus
Location in the file where the error or warning condition is
encountered.  Can be \fBNULL\fR if the condition occurred outside of
input file context.
.TP
.I err
This argument is \fB1\fR, if the function is called to report an error
condition and \fB0\fR otherwise (i.e. a warning).
.TP
.I errnum
System error code, or \fB0\fR, if no system error occurred.
.TP
.I msg
Formatted message.
.PP
The \fBgrecs_locus_t\fR structure consists of two \fBpoints\fR, each
point describing the beginning and the end of the input fragment in
question:
.sp
.nf
.in +5
struct grecs_locus_point {
     char *file;
     unsigned line;
     unsigned col;
};
typedef struct {
     struct grecs_locus_point beg;
     struct grecs_locus_point end;
} grecs_locus_t;
.in
.fi
.PP
The \fBfile\fR member points to the file name, and the \fBline\fR
member contains the input line number and the \fBcol\fR member
contains the column number. Both lines and columns are numbered from
1.
.PP
On output, the \fBgrecs_locus_t\fR structure is formatted as follows:
.sp
.nf
.in +5
\fBBEGFILE\fR:\fBBEGLINE\fR.\fBBEGCOL\fR\-\fBENDFILE\fR:\fBENDLINE\fR.\fBENDCOL\fR
.in
.fi
.PP
If \fBBEGFILE\fR equals \fBENDFILE\fR in the sense of
.BR strcmp (3),
then the output is contracted to:
.sp
.nf
.in +5
\fBFILE\fR:\fBBEGLINE\fR.\fBBEGCOL\fR\-\fBENDLINE\fR.\fBENDCOL\fR
.in
.fi
.PP
Furthermore, if \fBBEGLINE\fR equals \fBENDLINE\fR, then the format
is:
.sp
.nf
.in +5
\fBFILE\fR:\fBLINE\fR.\fBBEGCOL\fR\-\fBENDCOL\fR
.in
.fi
.PP
Finally, if \fBBEGCOL\fR\ equals \fBENDCOL\fR, the output is
simplified to
.sp
.nf
.in +5
\fBFILE\fR:\fBLINE\fR.\fBCOL\fR
.in
.fi
.PP
If \fBbeg.col\fR is \fB0\fR, then only the \fBbeg\fR part is
formatted:
.sp
.nf
.in +5
\fBFILE\fR:\fBLINE\fR
.in
.fi
.SH RETURN VALUE
None.
.SH "SEE ALSO"
.BR errno (3)
.SH AUTHORS
Sergey Poznyakoff
.SH "BUG REPORTS"
Report bugs to <gray+grecs@gnu.org.ua>.
.SH COLOPHON
The \fBGrecs\fR library is constantly changing, so this manual page
may be incorrect or out-of-date.  For the latest copy of \fBGrecs\fR
documentation, visit <http://www.gnu.org.ua/software/grecs>.
.SH COPYRIGHT
Copyright \(co 2011 Sergey Poznyakoff
.br
.na
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
.br
.ad
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
.\" Local variables:
.\" eval: (add-hook 'write-file-hooks 'time-stamp)
.\" time-stamp-start: ".TH [A-Z_][A-Z0-9_]* [0-9] \""
.\" time-stamp-format: "%:B %:d, %:y"
.\" time-stamp-end: "\""
.\" time-stamp-line-limit: 20
.\" end:

