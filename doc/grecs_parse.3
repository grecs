.\" This file is part of grecs -*- nroff -*-
.\" Copyright (C) 2007-2022 Sergey Poznyakoff
.\"
.\" Grecs is free software; you can redistribute it and/or modify
.\" it under the terms of the GNU General Public License as published by
.\" the Free Software Foundation; either version 3, or (at your option)
.\" any later version.
.\"
.\" Grecs is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.\" GNU General Public License for more details.
.\"
.\" You should have received a copy of the GNU General Public License
.\" along with Grecs.  If not, see <http://www.gnu.org/licenses/>.
.\"
.TH GRECS_PARSE 3 "July 1, 2011" "GRECS" "Grecs User Reference"
.SH NAME
grecs_parse \- parse a configuration file.
.SH SYNOPSIS
.nf
.B #include <grecs.h>
.sp
.BI "struct grecs_node *grecs_parse(const char *" "name" );
.SH DESCRIPTION
The
.BR grecs_parse ()
function reads the file \fBname\fR, which must be formatted
according to
.BR grecs_config (5)
and returns the parsed-out syntax tree.  On errors, \fBNULL\fR is
returned and appropriate diagnostic messages are printed using
.BR grecs_error (3).
.PP
The syntax tree consists of \fInodes\fR, linked together so as to
represent the file structure. A node is described by the following
object:
.sp
.nf
.in +5
typedef struct grecs_node {
     enum grecs_node_type type;
     grecs_locus_t locus;
     struct grecs_node *up;
     struct grecs_node *down;
     struct grecs_node *next;
     struct grecs_node *prev;
     char *ident;
     grecs_locus_t idloc;
     union {
          struct grecs_value *value;
          struct grecs_symtab *texttab;
     } v;
} grecs_node_t;
.in
.fi
.PP
The \fItype\fR member describes the type of this node.  Its value is
\fBgrecs_node_stmt\fR for simple statements and
\fBgrecs_node_block\fR, for block statements.  The topmost node (the
one returned by \fBgrecs_tree_parse\fR) has type \fBgrecs_node_root\fR.
.PP
The \fIlocus\fR describes the location in the input file, which this
node described.  See
.BR grecs_error (3),
for a description of \fBgrecs_locus_t\fR.
.PP
Adjacent nodes form a doubly-linked list using the \fBnext\fR and
\fBprev\fR pointers.  Thus, the node pointed to by \fBnext\fR
describes the statement that appears immediately after the one
described by the current node, whereas \fBprev\fR points to the node
describing a preceding statement.
.PP
If \fItype\fR is \fBgrecs_node_block\fR, the \fBdown\fR member points
to the first \fBsub-statement\fR in this block.  \fBdown->next\fR
will point to the second statement (if any), and so on.
.PP
Whatever the node type is, its \fBup\fR member will point to the node
corresponding to the upper-level block statement, if any.  In other
words, \fBdown\fR and \fBup\fR pointers reflect the hierarchical
structure of the file, so that the following holds true:
.sp
.nf
.in +5
node->up->down == node
node->down->up == node
.in
.fi
.sp
provided that \fBnode->up\fR and \fBnode->down\fR are not NULL.
.PP
The \fBident\fR member points to the keyword of that node.  For
example, a node corresponding to the input line:
.sp
.nf
.in +5
pidfile "/var/run/mypid";
.in
.fi
.sp
will have \fBident\fR pointing to the string \fB/var/run/mypid\fR.
.PP
The \fIidloc\fR contains the location of the identifier portion in the
input file. Notice, that it differs from \fBlocus\fR described above.
.PP
The \fBv\fR union keeps data which depend on the type of this node.
The \fBv.texttab\fR member is defined only for the root node (type
\fBgrecs_node_root\fR). It points to a symbol table which holds shared
strings for this tree. In particular, this table holds file names
referenced by \fBlocus\fR members of all underlying nodes.
.PP
For non-root nodes,the \fBv.value\fR member is defined.  It points to
the value associated with this statement.  A value is defined as:
.sp
.nf
.in +5
typedef struct grecs_value {
     int type;
     grecs_locus_t locus;
     union {
          struct grecs_list *list;
          char *string;
          struct {
               size_t c;
               struct grecs_value **v;
          } arg;
     } v;
} grecs_value_t;
.in
.fi
.PP
The \fBtype\fR will be \fBGRECS_TYPE_STRING\fR, for string values,
\fBGRECS_TYPE_LIST\fR, for list values, and \fBGRECS_TYPE_ARRAY\fR,
for arrays of values.  Depending on its value, the following members
of the union are used:
.TP
.B GRECS_TYPE_STRING
Actual string value is pointed to by \fBv.string\fR.
.TP
.B GRECS_TYPE_LIST
The list value is pointed to by \fBv.list\fR.
.TP
.B GRECS_TYPE_ARRAY
The array itself is stored in \fBv.arg.v\fR.  The \fBv.arg.c\fR member
contains the number of elements in the array.
.PP
The \fBlocus\fR member contains the location of this value in the
input file.
.SH RETURN VALUE
On success, a pointer to the root node.  On error, \fBNULL\fR.
.SH EXAMPLE
The following program parses the file and prints its contents on
screen:
.sp
.nf
.in +5
int
main(int argc, char **argv)
{
     struct grecs_node *tree;

     tree = grecs_parse(argv[1]);
     grecs_format_node(tree, GRECS_NODE_FLAG_DEFAULT, stdout);
     fputc('\\n', stdout);
     grecs_tree_free(tree);
     exit(0);
}
.in
.fi
.SH "SEE ALSO"
.BR grecs_config (5),
.BR grecs_error (3),
.BR grecs_format_node (3),
.BR grecs_tree_free (3).
.SH AUTHORS
Sergey Poznyakoff
.SH "BUG REPORTS"
Report bugs to <gray+grecs@gnu.org.ua>.
.SH COLOPHON
The \fBGrecs\fR library is constantly changing, so this manual page
may be incorrect or out-of-date.  For the latest copy of \fBGrecs\fR
documentation, visit <http://www.gnu.org.ua/software/grecs>.
.SH COPYRIGHT
Copyright \(co 2011 Sergey Poznyakoff
.br
.na
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
.br
.ad
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
.\" Local variables:
.\" eval: (add-hook 'write-file-hooks 'time-stamp)
.\" time-stamp-start: ".TH [A-Z_][A-Z0-9_]* [0-9] \""
.\" time-stamp-format: "%:B %:d, %:y"
.\" time-stamp-end: "\""
.\" time-stamp-line-limit: 20
.\" end:

