#ifdef HAVE_CONFIG_H
# include <config.h>
#endif
#include <stdlib.h>
#include <unistd.h>
#include "grecs.h"

int
main(int argc, char **argv)
{
	int trace = 0;
	int i;
	
	while ((i = getopt(argc, argv, "aI:p:t")) != EOF) {
		switch (i) {
		case 'a':
			grecs_parser_options |= GRECS_OPTION_ADJUST_STRING_LOCATIONS;
			break;
		case 'I':
			grecs_preproc_add_include_dir(optarg);
			break;
			
		case 'p':
			grecs_preprocessor = optarg;
			break;

		case 't':
			trace = 1;
			break;

		default:
			return 1;
		}
	}

	for (i = optind; i < argc; i++) {
		grecs_preprocess(argv[i], trace);
	}
	return 0;
}
