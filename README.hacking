Grecs README
Copyright (C) 2011-2022 Sergey Poznyakoff
See the end of file for copying conditions.

* Introduction

This file provides information on how to build a standalone
version of Grecs from sources obtained from Git.

For information about the standalone version, see the file
README.standalone in this directory.

For a detailed description of Grecs, see the documentation in
the doc/ subdirectory, or visit http://grecs.man.gnu.org.ua.

* Overview

Grecs is a library for parsing structured configuration files from
C programs. A structured configuration file has hierarchical
structure, with block statements enclosing lower-level
statements. Such configurations files are used by many programs, such
as, e.g. Bind or Dico.

Grecs provides primitives for parsing such files into an internal
tree-like structure and for basic operations on such structures. These
operations include value lookups by keyword paths, traversing trees
recursively, joining several trees together, reductions, etc.

* Requirements

If you have taken the sources from GIT you will need the following
packages to build the standalone library.  I don't make any extra
effort to accommodate older versions of these packages, so please make
sure that you have the latest stable version.

- Automake <http://www.gnu.org/software/automake/>
- Autoconf <http://www.gnu.org/software/autoconf/>
- Bison <http://www.gnu.org/software/bison/>
- Flex <http://flex.sourceforge.net/>
- M4 <http://www.gnu.org/software/m4/>
- Texinfo <http://www.gnu.org/software/texinfo>

* Bootstrapping

Obviously, if you are reading these notes, you did manage to check out
Grecs from GIT. The next step is to prepare the sources for standalone
build:

1. Create a placeholder for ChangeLog file:

  > ChangeLog

This file is needed for the bootstrap process.  It will be replaced
with the actual ChangeLog after make.

2. Bootstrap the package:

  autoreconf -f -i -s

3. Configure and make:

  ./configure
  make

* Bug reporting.		

Send bug reports to <gray+grecs@gnu.org.ua>. 


* Copyright information:

Copyright (C) 2011 Sergey Poznyakoff

   Permission is granted to anyone to make or distribute verbatim copies
   of this document as received, in any medium, provided that the
   copyright notice and this permission notice are preserved,
   thus giving the recipient permission to redistribute in turn.

   Permission is granted to distribute modified versions
   of this document, or of portions of it,
   under the above conditions, provided also that they
   carry prominent notices stating who last changed them.


Local Variables:
mode: outline
paragraph-separate: "[ 	]*$"
version-control: never
End:
